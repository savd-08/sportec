package soa4id.sportec;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.HashMap;

import soa4id.sportec.fragments.NavigationFragment;
import soa4id.sportec.infrastructure.AppFragmentFactory;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.Sport;
import soa4id.sportec.model.User;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentActivityComm {

    public User mUser;
    public String lastId;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    private AppSection currentFragmentId = AppSection.NO_SECTION;
    private SessionManager session;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        session = new SessionManager(getApplicationContext());
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (searchView != null) {
                    searchView.setIconified(true);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.requestFocus();
        searchView.setInputType(searchView.getInputType() | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
        return true;
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!searchView.isIconified()) {
            searchView.setIconified(true);
        } else {
            Bundle bundle = new Bundle();
            switch (currentFragmentId) {
                case NEWS:
                    super.onBackPressed();
                case STORY_DETAILS:
                    navigateToSection(AppSection.NEWS, false, null);
                    break;
                case SPORT_PROFILE:
                    navigateToSection(AppSection.SPORTS, false, null);
                    break;
                case USER_EDIT:
                    bundle.putString(User.USER_ID, mUser.getIdUser());
                    navigateToSection(AppSection.USER_PROFILE, false, bundle);
                    break;
                case TEAM:
                    bundle.putString(Sport.SPORT_ID, lastId);
                    navigateToSection(AppSection.SPORT_PROFILE, false, bundle);
                    break;
                default:
                    navigateToSection(AppSection.NEWS, false, null);
                    break;
            }
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_news:
                navigateToSection(AppSection.NEWS, true, null);
                break;
            case R.id.nav_profile:
                Bundle bundle = new Bundle();
                bundle.putString(User.USER_ID, mUser.getIdUser());
                navigateToSection(AppSection.USER_PROFILE, true, bundle);
                break;
            case R.id.nav_sports:
                navigateToSection(AppSection.SPORTS, true, null);
                break;
            case R.id.nav_close:
                session.logoutUser();
                finish();
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void navigateToSection(AppSection section, boolean fromNav, @Nullable Bundle args) {
        if (fromNav && currentFragmentId == section) {
            return;
        }
        if (section == AppSection.NO_SECTION) {
            section = AppSection.NEWS;
        }
        if (currentFragmentId == section) {
            return;
        }

        NavigationFragment fragment = AppFragmentFactory.getFragment(section);
        if (args != null) {
            fragment.setArguments(args);
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_fragment_container, fragment, fragment.getFragmentTag());
    /*if (addToStack)
        fragmentTransaction.addToBackStack(fragment.getFragmentTag());*/
        fragmentTransaction.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(NavigationFragment.FRAG_ID, currentFragmentId.ordinal());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentFragmentId = AppSection.values()[savedInstanceState
                .getInt(NavigationFragment.FRAG_ID)];
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        String userId = user.get(SessionManager.KEY_ID);
        String name = user.get(SessionManager.KEY_NAME);
        String imgUrl = user.get(SessionManager.KEY_IMG);
        String email = user.get(SessionManager.KEY_EMAIL);
        mUser = new User(new User.Builder(userId, email, "", name).setImgUrl(imgUrl));
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        TextView userNameTag = header.findViewById(R.id.nav_user_name);
        TextView userEmailTag = header.findViewById(R.id.nav_user_email);
        ImageView userProfilePic = header.findViewById(R.id.nav_image_view);
        userNameTag.setText(name);
        userEmailTag.setText(email);
        navigateToSection(currentFragmentId, false, null);
        Ion.with(userProfilePic)
                .placeholder(R.drawable.ic_account_box_24dp)
                .error(R.drawable.ic_error_image)
                .load(imgUrl);
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setNavSelection(int id) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(id);
    }

    @Override
    public void setNavDrawer(boolean setMenu) {
        if (setMenu) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            if (searchView != null) {
                searchView.setVisibility(View.VISIBLE);
                searchView.setIconified(true);
            }
            toggle.setDrawerIndicatorEnabled(true);
            toggle.syncState();
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            if (searchView != null) {
                searchView.setVisibility(View.GONE);
            }
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @Override
    public void setCurrentFragment(AppSection fragmentId) {
        currentFragmentId = fragmentId;
    }


}