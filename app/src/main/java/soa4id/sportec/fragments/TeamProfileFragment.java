package soa4id.sportec.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.Sport;
import soa4id.sportec.model.Team;

public class TeamProfileFragment extends NavigationFragment {
    private String mSportId;
    private String mSportName;
    private Team mTeam;
    private FragmentActivityComm mListener;

    public TeamProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSportId = getArguments().getString(Sport.SPORT_ID);
            mSportName = getArguments().getString(Sport.SPORT_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_team_profile, container, false);
        Button inviteButton = view.findViewById(R.id.invite);
        if (getArguments() != null) {
            mSportId = getArguments().getString(Sport.SPORT_ID);
            mSportName = getArguments().getString(Sport.SPORT_NAME);
        }
        view.setVisibility(View.INVISIBLE);
        JsonObject json = new JsonObject();
        json.addProperty("deporte", mSportName);
        json.addProperty("usuario", getMainActivity().mUser.getName());

        Ion.with(this.getContext())
                .load("GET", getResources().getString(R.string.teams) + "/sport")
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Gson gson = new Gson();
                            mTeam = gson.fromJson(result.toString(), Team.class);
                            updateUI(view, mTeam);
                        }
                    }
                });
        return view;
    }

    void updateUI(View view, Team team) {
        ImageView imageView = view.findViewById(R.id.profile_image);
        TextView nameText = view.findViewById(R.id.profile_name);
        TextView sportText = view.findViewById(R.id.team_sport);
        TextView membersText = view.findViewById(R.id.team_members);
        nameText.setText(mTeam.getName());
        sportText.setText(mTeam.getSport());
        membersText.setText(mTeam.getMembersString());
        Ion.with(imageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(team.getImgUrl());
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setScreenRotation(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Sport.SPORT_ID, mSportId);
        outState.putString(Sport.SPORT_NAME, mSportName);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mSportId = savedInstanceState.getString(Sport.SPORT_ID);
            mSportName = savedInstanceState.getString(Sport.SPORT_NAME);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivityComm) {
            mListener = (FragmentActivityComm) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentActivityComm");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
