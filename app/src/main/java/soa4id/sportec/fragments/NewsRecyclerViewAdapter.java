package soa4id.sportec.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.News;

/**
 * {@link RecyclerView.Adapter} that can display a {@link News} and makes a call to the
 * specified {@link soa4id.sportec.infrastructure.FragmentActivityComm}.
 */
public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder> {

    private static final String STORY_ID = "storyId";
    private final List<News> mValues;
    private final FragmentActivityComm mListener;

    public NewsRecyclerViewAdapter(List<News> items, FragmentActivityComm listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 0;
        else
            return 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_news_first, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_news, parent, false);
        }
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNewsTitleView.setText(mValues.get(position).getTitle());
        Ion.with(holder.mNewsImageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(mValues.get(position).getImageURL());
        final String id = mValues.get(position).getIdStory();
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    Bundle bundle = new Bundle();
                    bundle.putString(STORY_ID, id);
                    mListener.navigateToSection(AppSection.STORY_DETAILS, false, bundle);
                }
            }
        });

        if (holder.type) {
            Calendar cNow = Calendar.getInstance();
            int nowDay = cNow.get(Calendar.DAY_OF_MONTH);
            int nowMonth = cNow.get(Calendar.MONTH);
            int nowYear = cNow.get(Calendar.YEAR);
            Calendar storyDate = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
            try {
                storyDate.setTime(sdf.parse(mValues.get(position).getDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int storyDay = storyDate.get(Calendar.DAY_OF_MONTH);
            int storyMonth = storyDate.get(Calendar.MONTH);
            int storyYear = storyDate.get(Calendar.YEAR);
            int storyHour = storyDate.get(Calendar.HOUR_OF_DAY);
            int storyMinute = storyDate.get(Calendar.MINUTE);
            String storyMonthStr = storyDate.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
            String dateStr = storyDay + " de " + storyMonthStr;
            if (nowYear == storyYear) {
                if (nowMonth == storyMonth) {
                    if (nowDay == storyDay) {
                        dateStr = "Hoy";
                    } else if (nowDay == (storyDay + 1)) {
                        dateStr = "Ayer";
                    }
                }
            } else {
                dateStr += " de " + storyYear;
            }
            dateStr += " a las " + ((storyHour < 10) ? "0" + storyHour : storyHour) + ":"
                    + ((storyMinute < 10) ? "0" + storyMinute : storyMinute);
            holder.mNewsDateView.setText(dateStr);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mNewsTitleView;
        public final TextView mNewsDateView;
        public final ImageView mNewsImageView;
        public final View mView;
        public News mItem;
        public boolean type;

        public ViewHolder(View view, int type) {
            super(view);
            mView = view;
            this.type = (type == 1);
            mNewsTitleView = (TextView) view.findViewById(R.id.news_title);
            if (this.type) {
                mNewsDateView = (TextView) view.findViewById(R.id.news_date);
            } else {
                mNewsDateView = mNewsTitleView;
            }
            mNewsImageView = (ImageView) view.findViewById(R.id.news_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNewsTitleView.getText() + "'";
        }
    }
}
