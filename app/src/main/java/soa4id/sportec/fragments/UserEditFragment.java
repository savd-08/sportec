package soa4id.sportec.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.nio.charset.Charset;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.User;

public class UserEditFragment extends NavigationFragment {
    EditText _emailText;
    EditText _nameText;
    EditText _passwordText;
    EditText _reEnterPasswordText;
    Button _editButton;
    private String mUserId;
    private User mUser;
    private FragmentActivityComm mListener;

    public UserEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(User.USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_user_edit, container, false);
        _nameText = view.findViewById(R.id.input_name);
        _emailText = view.findViewById(R.id.input_email);
        _passwordText = view.findViewById(R.id.input_password);
        _reEnterPasswordText = view.findViewById(R.id.input_reEnterPassword);
        _editButton = view.findViewById(R.id.btn_edit);
        if (getArguments() != null) {
            mUserId = getArguments().getString(User.USER_ID);
        }
        view.setVisibility(View.INVISIBLE);
        Ion.with(this.getContext())
                .load(getResources().getString(R.string.users) + "/" + mUserId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Gson gson = new Gson();
                            mUser = gson.fromJson(result.toString(), User.class);
                            updateUI(view, mUser);
                        }
                    }
                });
        _editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUser();
            }
        });
        return view;
    }

    public void onEditSuccess() {
        _editButton.setEnabled(true);
        Bundle bundle = new Bundle();
        bundle.putString(User.USER_ID, mUser.getIdUser());
        mListener.navigateToSection(AppSection.USER_PROFILE, false, bundle);
    }

    public void onEditFailed() {
        Snackbar.make(this.getMainActivity().findViewById(R.id.scroll_view), "No se pudo modificar cuenta", Snackbar.LENGTH_LONG)
                .show();
        _editButton.setEnabled(true);
    }

    public void editUser() {
        if (!validate()) {
            onEditFailed();
            return;
        }
        _editButton.setEnabled(false);
        final ProgressDialog progressDialog = new ProgressDialog(this.getMainActivity(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Modificando cuenta...");
        progressDialog.show();
        String name = _nameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        if (password.isEmpty()) {
            password = mUser.getPassword();
        } else {
            HashCode hashCode = Hashing.sha256().hashString(password, Charset.defaultCharset());
            password = hashCode.toString();
        }
        JsonObject json = new JsonObject();
        json.addProperty("nombre", name.trim());
        json.addProperty("email", email.trim());
        json.addProperty("password", password);
        json.addProperty("imgUrl", mUser.getImgUrl());
        json.addProperty("deportes", new Gson().toJson(mUser.getSports()));
        //PUT edit
        Ion.with(this)
                .load("PUT", getResources().getString(R.string.users) + "/" + mUserId)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        progressDialog.dismiss();
                        if (result.has("error")) {
                            onEditFailed();
                        } else {
                            onEditSuccess();
                        }
                    }
                });
    }

    void updateUI(View view, User user) {
        ImageView imageView = view.findViewById(R.id.usr_image);
        EditText nameText = view.findViewById(R.id.input_name);
        EditText emailText = view.findViewById(R.id.input_email);
        nameText.setText(mUser.getName());
        emailText.setText(mUser.getEmail());
        Ion.with(imageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(user.getImgUrl());
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setScreenRotation(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(User.USER_ID, mUserId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mUserId = savedInstanceState.getString(User.USER_ID);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivityComm) {
            mListener = (FragmentActivityComm) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentActivityComm");
        }
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("Debe contener por lo menos 3 caracteres");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("Dirección de correo electrónico inválida");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if ((!password.isEmpty() && password.length() < 8) || password.length() > 16) {
            _passwordText.setError("Debe contener entre 8 y 16 caracteres alfa numéricos");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if ((!reEnterPassword.isEmpty() && reEnterPassword.length() < 8)
                || reEnterPassword.length() > 16 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError("Las contraseñas no coinciden");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
