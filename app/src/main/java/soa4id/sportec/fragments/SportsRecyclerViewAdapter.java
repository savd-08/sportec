package soa4id.sportec.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.Sport;


public class SportsRecyclerViewAdapter extends RecyclerView.Adapter<SportsRecyclerViewAdapter.ViewHolder> {
    private final List<Sport> mValues;
    private final FragmentActivityComm mListener;

    public SportsRecyclerViewAdapter(List<Sport> items, FragmentActivityComm listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_sports, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNameView.setText(mValues.get(position).getName());
        Ion.with(holder.mImageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(mValues.get(position).getImageUrl());

        final String id = mValues.get(position).getIdSport();
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    Bundle bundle = new Bundle();
                    bundle.putString(Sport.SPORT_ID, id);
                    mListener.navigateToSection(AppSection.SPORT_PROFILE, false, bundle);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final ImageView mImageView;
        public Sport mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.sport_name);
            mImageView = (ImageView) view.findViewById(R.id.sport_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
