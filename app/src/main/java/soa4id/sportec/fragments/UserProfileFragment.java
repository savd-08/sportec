package soa4id.sportec.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.User;

public class UserProfileFragment extends NavigationFragment {
    private String mUserId;
    private User mUser;
    private FragmentActivityComm mListener;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(User.USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        Button editButton = view.findViewById(R.id.user_edit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(User.USER_ID, mUser.getIdUser());
                mListener.navigateToSection(AppSection.USER_EDIT, false, bundle);
            }
        });
        if (getArguments() != null) {
            mUserId = getArguments().getString(User.USER_ID);
        }
        view.setVisibility(View.INVISIBLE);
        Ion.with(this.getContext())
                .load(getResources().getString(R.string.users) + "/" + mUserId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Gson gson = new Gson();
                            mUser = gson.fromJson(result.toString(), User.class);
                            updateUI(view, mUser);
                        }
                    }
                });
        return view;
    }

    void updateUI(View view, User user) {
        ImageView imageView = view.findViewById(R.id.profile_image);
        TextView nameText = view.findViewById(R.id.profile_name);
        TextView emailText = view.findViewById(R.id.user_email);
        TextView preferencesText = view.findViewById(R.id.user_preferences);
        nameText.setText(mUser.getName());
        emailText.setText(mUser.getEmail());
        preferencesText.setText(mUser.getSportsString());
        Ion.with(imageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(user.getImgUrl());
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setScreenRotation(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(User.USER_ID, mUserId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mUserId = savedInstanceState.getString(User.USER_ID);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivityComm) {
            mListener = (FragmentActivityComm) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentActivityComm");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
