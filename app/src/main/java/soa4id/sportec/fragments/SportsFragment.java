package soa4id.sportec.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.Sport;


public class SportsFragment extends NavigationFragment {

    private int mColumnCount = 2;
    private FragmentActivityComm mListener;
    private List<Sport> mSportsList = new ArrayList<Sport>();

    public SportsFragment() {
    }

    public static SportsFragment newInstance(int columnCount) {
        SportsFragment fragment = new SportsFragment();
        Bundle args = new Bundle();
        ;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sports_list, container, false);
        view.setVisibility(View.INVISIBLE);

        //GET Sports List
        Ion.with(this.getContext())
                .load(getResources().getString(R.string.sports))
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (result != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<Sport>>() {
                            }.getType();
                            mSportsList = gson.fromJson(result.toString(), type);
                            if (view instanceof RecyclerView) {
                                Context context = view.getContext();
                                RecyclerView recyclerView = (RecyclerView) view;
                                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                                recyclerView.setAdapter(new SportsRecyclerViewAdapter(mSportsList, mListener));
                            }
                            view.setVisibility(View.VISIBLE);
                        }
                    }
                });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setScreenRotation(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivityComm) {
            mListener = (FragmentActivityComm) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentActivityComm");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
