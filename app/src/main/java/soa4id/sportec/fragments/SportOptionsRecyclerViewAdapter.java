package soa4id.sportec.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.Sport;
import soa4id.sportec.model.SportOption;


public class SportOptionsRecyclerViewAdapter extends RecyclerView.Adapter<SportOptionsRecyclerViewAdapter.ViewHolder> {
    private final FragmentActivityComm mListener;
    private final List<SportOption> mValues = new ArrayList<>();
    private final String mSport;

    public SportOptionsRecyclerViewAdapter(FragmentActivityComm listener, String sport) {
        mListener = listener;
        mValues.add(new SportOption(new SportOption.Builder("0", "Resultados")));
        mValues.add(new SportOption(new SportOption.Builder("1", "Equipo")));
        mValues.add(new SportOption(new SportOption.Builder("2", "Retos")));
        mSport = sport;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_sport_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        final int pos = position;
        holder.mNameView.setText(mValues.get(position).getName());
        switch (pos) {
            case 0:
                holder.mImageView.setImageResource(R.drawable.ic_list);
                break;
            case 1:
                holder.mImageView.setImageResource(R.drawable.ic_team);
                break;
            case 2:
                holder.mImageView.setImageResource(R.drawable.ic_flag);
                break;
        }

        final String id = mValues.get(position).getIdSport();
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    Bundle bundle = new Bundle();
                    bundle.putString(Sport.SPORT_ID, id);
                    bundle.putString(Sport.SPORT_NAME, mSport);
                    switch (pos) {
                        case 0:
//                            mListener.navigateToSection(AppSection.RESULTS, false, bundle);
                            break;
                        case 1:
                            mListener.navigateToSection(AppSection.TEAM, false, bundle);
                            break;
                        case 2:
//                            mListener.navigateToSection(AppSection.CHALLENGES, false, bundle);
                            break;
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final ImageView mImageView;
        public SportOption mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.option_name);
            mImageView = (ImageView) view.findViewById(R.id.option_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
