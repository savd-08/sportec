package soa4id.sportec.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.News;


public class StoryDetailsFragment extends NavigationFragment {

    private String mStoryId;
    private News mStory;

    private FragmentActivityComm mListener;

    public StoryDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStoryId = getArguments().getString(News.STORY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getArguments() != null) {
            mStoryId = getArguments().getString(News.STORY_ID);
        }
        final View view = inflater.inflate(R.layout.fragment_story_details, container, false);
        view.setVisibility(View.INVISIBLE);
        //GET Story
        Ion.with(this.getContext())
                .load(getResources().getString(R.string.news) + "/" + mStoryId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Gson gson = new Gson();
                            mStory = gson.fromJson(result.toString(), News.class);
                            updateUI(view, mStory);
                        }
                    }
                });
        return view;
    }

    void updateUI(View view, News news) {
        Calendar storyDate = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        try {
            storyDate.setTime(sdf.parse(news.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int storyDay = storyDate.get(Calendar.DAY_OF_MONTH);
        int storyYear = storyDate.get(Calendar.YEAR);
        int storyHour = storyDate.get(Calendar.HOUR_OF_DAY);
        int storyMinute = storyDate.get(Calendar.MINUTE);
        String storyDayStr = storyDate.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
        String storyMonthStr = storyDate.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());

        TextView dateView = view.findViewById(R.id.story_date);
        TextView titleView = view.findViewById(R.id.story_title);
        TextView descView = view.findViewById(R.id.story_description);
        ImageView imageView = view.findViewById(R.id.story_image);
        Ion.with(imageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(news.getImageURL());
        String dateStr = storyDayStr.toUpperCase() + " " + storyMonthStr.toUpperCase() + " "
                + storyDay + ", " + storyYear + " / "
                + ((storyHour < 10) ? "0" + storyHour : storyHour) + ":"
                + ((storyMinute < 10) ? "0" + storyMinute : storyMinute);
        titleView.setText(news.getTitle());
        dateView.setText(dateStr);
        descView.setText(news.getDescription());
        view.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        setScreenRotation(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(News.STORY_ID, mStoryId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mStoryId = savedInstanceState.getString(News.STORY_ID);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivityComm) {
            mListener = (FragmentActivityComm) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentActivityComm");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
