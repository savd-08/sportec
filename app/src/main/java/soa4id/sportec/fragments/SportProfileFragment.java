package soa4id.sportec.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import soa4id.sportec.R;
import soa4id.sportec.infrastructure.FragmentActivityComm;
import soa4id.sportec.model.Sport;

public class SportProfileFragment extends NavigationFragment {
    private String mSportId;
    private Sport mSport;
    private FragmentActivityComm mListener;

    public SportProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSportId = getArguments().getString(Sport.SPORT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_sport_profile, container, false);
        if (getArguments() != null) {
            mSportId = getArguments().getString(Sport.SPORT_ID);
        }
        view.setVisibility(View.INVISIBLE);
        Ion.with(this.getContext())
                .load(getResources().getString(R.string.sports) + "/" + mSportId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Gson gson = new Gson();
                            mSport = gson.fromJson(result.toString(), Sport.class);
                            updateUI(view, mSport);
                        }
                    }
                });
        return view;
    }

    void updateUI(View view, Sport sport) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.options_recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
        recyclerView.setAdapter(new SportOptionsRecyclerViewAdapter(mListener, mSport.getName()));
        ImageView imageView = view.findViewById(R.id.profile_image);
        TextView nameText = view.findViewById(R.id.profile_name);
        nameText.setText(mSport.getName());
        Ion.with(imageView)
                .placeholder(R.drawable.ic_menu_gallery)
                .error(R.drawable.ic_error_image)
                .load(sport.getImageUrl());
        getMainActivity().lastId = mSportId;
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setScreenRotation(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Sport.SPORT_ID, mSportId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mSportId = savedInstanceState.getString(Sport.SPORT_ID);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentActivityComm) {
            mListener = (FragmentActivityComm) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentActivityComm");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
