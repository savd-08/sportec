package soa4id.sportec.fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import soa4id.sportec.MainActivity;
import soa4id.sportec.infrastructure.AppSection;
import soa4id.sportec.infrastructure.FragmentActivityComm;

/**
 * Not generic.
 * Created by amora on 10/17/17.
 */

public abstract class NavigationFragment extends Fragment {

    public static final String TITLE_ID = "titleId";
    public static final String NAV_ID = "navId";
    public static final String FRAG_ID = "fragmentId";
    public static final String MENU_STATUS = "menuStatus";
    protected FragmentActivityComm mParent;
    protected int mNavigationId;
    protected int mTitleId;
    protected AppSection mFragmentId;
    protected boolean mShowMenu;

    protected NavigationFragment() {
    }

    public String getFragmentTag() {
        return this.getClass().getSimpleName();
    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        mParent.setTitle(getString(mTitleId));
        mParent.setNavSelection(mNavigationId);
        mParent.setCurrentFragment(mFragmentId);
        mParent.setNavDrawer(mShowMenu);
        super.onResume();
    }

    public void setScreenRotation(boolean allow) {
        if (allow) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_USER);
        } else {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TITLE_ID, mTitleId);
        outState.putInt(NAV_ID, mNavigationId);
        outState.putInt(FRAG_ID, mFragmentId.ordinal());
        outState.putBoolean(MENU_STATUS, mShowMenu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mTitleId = savedInstanceState.getInt(TITLE_ID);
            mNavigationId = savedInstanceState.getInt(NAV_ID);
            mFragmentId = AppSection.values()[savedInstanceState.getInt(FRAG_ID)];
            mShowMenu = savedInstanceState.getBoolean(MENU_STATUS);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    public void setBuilder(Builder pBuilder) {
        this.mNavigationId = pBuilder.mNavigationId;
        this.mTitleId = pBuilder.mTitleId;
        this.mShowMenu = pBuilder.mShowMenu;
    }

    public AppSection getFragmentId() {
        return mFragmentId;
    }

    public void setFragmentId(AppSection fragmentId) {
        this.mFragmentId = fragmentId;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mParent = (FragmentActivityComm) context;
    }

    public static class Builder {

        protected int mNavigationId;

        protected int mTitleId;

        protected boolean mShowMenu = true;

        public Builder(int pNavigationId) {
            this.mNavigationId = pNavigationId;
        }

        public Builder setTitle(int pTitle) {
            this.mTitleId = pTitle;
            return this;
        }

        public Builder setMenu(boolean showMenu) {
            this.mShowMenu = showMenu;
            return this;
        }
    }
}