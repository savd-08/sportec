package soa4id.sportec.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sergio on 18/10/17.
 */

public class News {

    @Expose(serialize = false, deserialize = false)
    public static final String STORY_ID = "storyId";

    @SerializedName("id")
    private String idStory;
    @SerializedName("cuerpo")
    private String description;
    @SerializedName("titulo")
    private String title;
    @SerializedName("fecha")
    private String date;
    @SerializedName("imgUrl")
    private String imageURL;

    public News() {

    }

    public News(Builder builder) {
        this.idStory = builder.idStory;
        this.description = builder.description;
        this.title = builder.title;
        this.date = builder.date;
        this.imageURL = builder.imageURL;
    }

    public String getIdStory() {
        return idStory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public static class Builder {
        private final String idStory;
        private String description;
        private String title;
        private String date;
        private String imageURL;

        public Builder(String idStory) {
            this.idStory = idStory;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setDate(String date) {
            this.date = date;
            return this;
        }

        public Builder setImageURL(String imageURL) {
            this.imageURL = imageURL;
            return this;
        }
    }
}
