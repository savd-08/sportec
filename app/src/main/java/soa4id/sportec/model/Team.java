package soa4id.sportec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sergio on 21/10/17.
 */

public class Team {
    @Expose(serialize = false, deserialize = false)
    public static final String TEAM_ID = "teamId";


    @SerializedName("id")
    private String idTeam;
    @SerializedName("nombre")
    private String name;
    @SerializedName("deporte")
    private String sport;
    @SerializedName("imgUrl")
    private String imgUrl;
    @SerializedName("miembros")
    private List<String> members;

    public Team() {
    }


    public Team(Builder builder) {
        this.idTeam = builder.idTeam;
        this.sport = builder.sport;
        this.name = builder.name;
        this.imgUrl = builder.imgUrl;
        this.members = builder.members;
    }

    public String getIdTeam() {
        return idTeam;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public List<String> getMembers() {
        return members;
    }

    public String getMembersString() {
        StringBuilder sb = new StringBuilder();
        for (String s : this.members) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public String getSport() {
        return sport;
    }

    public static class Builder {
        private final String idTeam;
        private final String name;
        private final String sport;
        private List<String> members;
        private String imgUrl;

        public Builder(String idUser, String email, String name) {
            this.idTeam = idUser;
            this.sport = email;
            this.name = name;
        }

        public Builder setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
            return this;
        }

        public Builder setMembers(List<String> members) {
            this.members = members;
            return this;
        }
    }
}
