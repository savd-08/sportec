package soa4id.sportec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sergio on 21/10/17.
 */

public class User {
    @Expose(serialize = false, deserialize = false)
    public static final String USER_ID = "userId";

    @SerializedName("id")
    private String idUser;
    @SerializedName("nombre")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("imgUrl")
    private String imgUrl;
    @SerializedName("deportes")
    private List<String> sports;

    public User() {
    }


    public User(Builder builder) {
        this.idUser = builder.idUser;
        this.email = builder.email;
        this.name = builder.name;
        this.password = builder.password;
        this.imgUrl = builder.imgUrl;
        this.sports = builder.sports;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public List<String> getSports() {
        return sports;
    }

    public String getSportsString() {
        StringBuilder sb = new StringBuilder();
        for (String s : this.sports) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public static class Builder {
        private final String idUser;
        private final String name;
        private final String email;
        private final String password;
        private List<String> sports;
        private String imgUrl;

        public Builder(String idUser, String email, String password, String name) {
            this.idUser = idUser;
            this.email = email;
            this.password = password;
            this.name = name;
        }

        public Builder setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
            return this;
        }

        public Builder setSports(List<String> sports) {
            this.sports = sports;
            return this;
        }
    }
}
