package soa4id.sportec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sergio on 25/11/17.
 */

public class SportOption {
    @Expose(serialize = false, deserialize = false)
    public static final String OPT_ID = "sportId";

    @SerializedName("id")
    private String idOption;
    @SerializedName("nombre")
    private String name;

    public SportOption(Builder builder) {
        this.idOption = builder.idSport;
        this.name = builder.name;
    }

    public String getIdSport() {
        return idOption;
    }

    public String getName() {
        return name;
    }

    public static class Builder {
        private final String idSport;
        private final String name;

        public Builder(String idSport, String name) {
            this.idSport = idSport;
            this.name = name;
        }
    }
}
