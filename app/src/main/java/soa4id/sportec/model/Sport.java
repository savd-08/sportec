package soa4id.sportec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sergio on 25/11/17.
 */

public class Sport {
    @Expose(serialize = false, deserialize = false)
    public static final String SPORT_ID = "sportId";
    @Expose(serialize = false, deserialize = false)
    public static final String SPORT_NAME = "sportName";

    @SerializedName("id")
    private String idSport;
    @SerializedName("imgUrl")
    private String imageUrl;
    @SerializedName("nombre")
    private String name;

    public Sport(Builder builder) {
        this.idSport = builder.idSport;
        this.imageUrl = builder.imagUrl;
        this.name = builder.name;
    }

    public String getIdSport() {
        return idSport;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public static class Builder {
        private final String idSport;
        private final String name;
        private final String imagUrl;

        public Builder(String idSport, String name, String imagUrl) {
            this.idSport = idSport;
            this.name = name;
            this.imagUrl = imagUrl;
        }
    }
}
