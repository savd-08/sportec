package soa4id.sportec.infrastructure;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by sergio on 18/10/17.
 */

public interface FragmentActivityComm {
    void setTitle(String title);

    void setNavSelection(int id);

    void setNavDrawer(boolean setMenu);

    void setCurrentFragment(AppSection fragmentId);

    void navigateToSection(AppSection section, boolean fromNav, @Nullable Bundle args);
}
