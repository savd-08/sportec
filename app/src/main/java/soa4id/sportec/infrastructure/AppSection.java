package soa4id.sportec.infrastructure;

/**
 * Created by amora on 10/17/17.
 */

public enum AppSection {
    NO_SECTION,
    NEWS,
    STORY_DETAILS,
    USER_PROFILE,
    USER_EDIT,
    SPORTS,
    SPORT_PROFILE,
    TEAM,
    CHALLENGES,
    RESULTS
}
