package soa4id.sportec.infrastructure;

import soa4id.sportec.R;
import soa4id.sportec.fragments.NavigationFragment;
import soa4id.sportec.fragments.NewsFragment;
import soa4id.sportec.fragments.SportProfileFragment;
import soa4id.sportec.fragments.SportsFragment;
import soa4id.sportec.fragments.StoryDetailsFragment;
import soa4id.sportec.fragments.TeamProfileFragment;
import soa4id.sportec.fragments.UserEditFragment;
import soa4id.sportec.fragments.UserProfileFragment;

public class AppFragmentFactory {

    public static NavigationFragment getFragment(AppSection pNavigationFragmentType) {
        NavigationFragment newFragment;

        switch (pNavigationFragmentType) {
            case NEWS:
                newFragment = new NewsFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_news)
                        .setTitle(R.string.news_section));
                break;
            case STORY_DETAILS:
                newFragment = new StoryDetailsFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_news)
                        .setTitle(R.string.news_section).setMenu(false));
                break;
            case USER_PROFILE:
                newFragment = new UserProfileFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_profile)
                        .setTitle(R.string.profile_section));
                break;
            case USER_EDIT:
                newFragment = new UserEditFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_profile)
                        .setTitle(R.string.profile_section).setMenu(false));
                break;
            case SPORTS:
                newFragment = new SportsFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_sports)
                        .setTitle(R.string.sports_section));
                break;
            case SPORT_PROFILE:
                newFragment = new SportProfileFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_sports)
                        .setTitle(R.string.sports_section).setMenu(false));
                break;
            case TEAM:
                newFragment = new TeamProfileFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_sports)
                        .setTitle(R.string.sports_section).setMenu(false));
                break;
            default:
                newFragment = new NewsFragment();
                newFragment.setBuilder(new NavigationFragment.Builder(R.id.nav_news)
                        .setTitle(R.string.app_name));
                break;
        }
        newFragment.setFragmentId(pNavigationFragmentType);
        return newFragment;
    }

}
