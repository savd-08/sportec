#Curso SOA4ID

### Proyecto Final

###### Estudiante

* Sergio Vargas 

###### Requerimientos
* Android Studio 3.0.1
* Java 1.8.0_112_release-b06 amd64
* SDK mínimo24 (Android 7.0 Nougat)

###### Instalación

Se debe clonar el repositorio git.
En Linux, generalmente se puede utilizar
~~~~
https://savd-08@bitbucket.org/savd-08/sportec.git
~~~~
Es preferible tener instalado Android Studio, sin embargo el archivo APK de instalación se encuentra en el respositorio release.